
$(document).ready(function () {

  var focus = true;
  
  /* Load first content */
  changeContent('/header/1','headerContent', false);
  changeContent('/content/1','contentContent', false);
  changeContent('/footer/1','footerContent', false);

  setTimeout(function(){
    makeItFocus(true);
    $('#tourButton').on('click', function(event){
      focus = false;
      bootstro.start('.bootstro', {
        onStep: function(params){
          if (params.idx == 4 && params.direction == 'next'){
            $('.swappingLeft.header').trigger('mouseenter');
          }
          if (params.idx == 5 && params.direction == 'next') {

            $('.swappingLeft.header').trigger('mouseleave');
            $('.swappingLeft.content').trigger('mouseenter');
          }
          if (params.idx == 6 && params.direction == 'next') {
            $('.swappingLeft.content').trigger('mouseleave');
            $('.swappingLeft.header').trigger('mouseenter');
            changeContent('/content/2','contentContent', false);
          }
          if (params.idx == 7 && params.direction == 'next'){            
            $('.swappingLeft.header').trigger('mouseleave');
            changeContent('/header/2','headerContent', false);
          }
        }
      });
    });
  }, 1500);

  function makeItFocus(bool){
    setTimeout(function() { 
      if(bool && focus) $('#tourButton').css("transform", "scale(1.02, 1.02)");
      else $('#tourButton').css("transform", "scale(0.98, 0.98)");
      if(focus) makeItFocus(!bool);
      else $('#tourButton').css("transform", "scale(1, 1)");
    }, 500);
  }

  $(window).resize(function() {
    var h1 = $("#headerContainer").prop('scrollHeight');
    var h2 = $("#contentContainer").prop('scrollHeight');
    var h3 = $("#footerContainer").prop('scrollHeight');

    $("#headerContent").css('height', h1);
    $("#contentContent").css('height', h2);
    $("#footerContent").css('height', h3);
  });

  /* Events */

  $('.modal').on('show.bs.modal', function () {
      if ($(document).height() > $(window).height()) {
          // no-scroll
          $('body').addClass("modal-open-noscroll");
      }
      else {
          $('body').removeClass("modal-open-noscroll");
      }
  });
  $('.modal').on('hide.bs.modal', function () {
      $('body').removeClass("modal-open-noscroll");
  });

  $('#swappings').children().each(function(){
    $(this).on('click', function(event){
        event.preventDefault();
        //detect which page has been selected
        var type = $(this).attr('data-type');
        var index = $('#'+type+'Container').attr('data-element-index');
        if ($(this).hasClass('swappingLeft')) index--;
        else index++;
        if (index > 5) index = 1;
        if (index < 1) index = 5;
        $('#'+type+'Container').attr('data-element-index', index);
        var url = '/'+type+'/'+index;
        changeContent(url, type+'Content', true);
        setTimeout(function(){
          fadeOutOff(type);
        }, 800);
    });

    $(this)
      .on( "mouseenter", function() {
        var element =  $(this).attr('class').split(" ")[1];
        $('html, body').animate({
          scrollTop: $('#'+element+'Container').offset().top - 20
        }, 500);
        fadeOutOn(element);
      })
      .on( "mouseleave", function() {
        var element =  $(this).attr('class').split(" ")[1];
        fadeOutOff(element);
      });
  });

  function fadeOutOff(element){
    switch (element) {
      case 'header':
        $('#contentContainer').removeClass('faded');
        $('#footerContainer').removeClass('faded');
      case 'content':            
        $('#headerContainer').removeClass('faded');
        $('#footerContainer').removeClass('faded');
      case 'footer':
        $('#headerContainer').removeClass('faded');
        $('#contentContainer').removeClass('faded');
      default:
        $('#'+element+'Container').removeClass('highlight');
    }
  }

  function fadeOutOn(element){
    switch (element) {
      case 'header':
        $('#contentContainer').addClass('faded');
        $('#footerContainer').addClass('faded');
        break;
      case 'content':            
        $('#headerContainer').addClass('faded');
        $('#footerContainer').addClass('faded');
        break;
      case 'footer':
        $('#headerContainer').addClass('faded');
        $('#contentContainer').addClass('faded');
        break;
    }
    $('#'+element+'Container').addClass('highlight');
  }

  function changeContent(url, target, bool) {
    // trigger page animation
    $('#'+target).addClass('page-is-changing');
    $('#'+target).children('.cd-loading-bar').one('webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend', function(){
      loadNewContent(url, target, bool);
      newLocation = url;
      $('#'+target).children('.cd-loading-bar').off('webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend');
    });
    loadNewContent(url, target, bool);
  }

  function loadNewContent(url, target, bool) {
    setTimeout(function(){
      $('#'+target).load(url, function(event){

        if (url=='/content/2' || url=='/content/3' || url=='/content/4') {
          $('#'+target).css('margin-top', -150);
        }
        else {
          $('#'+target).css('margin-top', 0);
        }
        
        $(this).wrapInner('<div>');
        var newheight = $('div:first',this).height();
        $(this).animate( {height: newheight} );
        //if browser doesn't support CSS transitions - dont wait for the end of transitions
        setTimeout(function(){
          $('#'+target).removeClass('page-is-changing');
          //wait for the end of the transition on the loading bar before revealing the new content
          $('#'+target).children('.cd-loading-bar').one('webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend', function(){
            isAnimating = false;
            $('#'+target).children('.cd-loading-bar').off('webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend');
          });
          reloadEvents();
        }, 400);
    
      });
    }, 800);
  }

  function reloadEvents(){
    $('.carousel').carousel({
        interval: 0 //changes the speed
    })
  }

  /* Helpers */

  var getCSS = function (prop, fromClass) {

    var $inspector = $("<div>").css('display', 'none').addClass(fromClass);
    $("body").append($inspector); // add to DOM, in order to read the CSS property
    try {
        return $inspector.css(prop);
    } finally {
        $inspector.remove(); // and remove from DOM
    }
  };

  
});