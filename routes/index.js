var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Landing page Examples', index: 1 });
});

var components = ['header', 'content', 'footer']
components.forEach(function(element){
  router.get('/'+element+'/:id', function(req, res, next) {
    var id = req.params.id;
    res.render(element+'s/'+element+'-'+id, { title: element, index : id });
  });
});

console.log(router.stack);

module.exports = router;